import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import {fadeIn} from 'react-navigation-transitions';
import Home from '../screens/Home';
import AddDevice from '../screens/AddDevice';
import HomeDevice from '../screens/HomeDevice';
import SettingsDevice from '../screens/SettingsDevice';

const RootStack = createStackNavigator(
  {
    Home: {
      screen: Home,
    },
    AddDevice: {
      screen: AddDevice,
    },
    HomeDevice: {
      screen: HomeDevice,
    },
    SettingsDevice: {
      screen: SettingsDevice,
    },
  },
  {
    initialRouteName: 'Home',
    transitionConfig: () => fadeIn(),
    headerMode: 'none',
  },
);

const Navigator = createAppContainer(RootStack);

export default Navigator;
