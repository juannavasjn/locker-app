import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Bluetooth from '../components/Bluetooth';
import AddId from '../components/AddId';
import Header from '../components/Header';
import Footer from '../components/Footer';
import {vw, vh} from 'react-native-expo-viewport-units';

const AddDevice = props => {
  const [state, setState] = useState({
    ble: true,
    id: false,
    colorBle: 'blue',
    colorId: '',
  });

  const [tabs, setTabs] = useState(['textAct', 'text']);

  // const handlePress = type => {
  // 	if (type === 'ble') {
  // 		setState({
  // 			ble: true,
  // 			id: false,
  // 			colorBle: 'blue',
  // 			colorId: ''
  // 		});
  // 	} else if (type === 'id') {
  // 		setState({
  // 			ble: false,
  // 			id: true,
  // 			colorBle: '',
  // 			colorId: 'blue'
  // 		});
  // 	}
  // };

  const handlePress = type => {
    switch (type) {
      case 'ble':
        setTabs(['textAct', 'text']);
        break;
      case 'id':
        setTabs(['text', 'textAct']);
        break;
      default:
        break;
    }
  };

  return (
    <View style={styles.container}>
      <Header title={'Añadir Puerta'} />
      <Footer navigation={props.navigation} />
      <View style={styles.content}>
        <View style={styles.row1}>
          <TouchableOpacity onPress={_ => handlePress('ble')}>
            <View style={styles.col}>
              <Text style={styles[tabs[0]]}>Dispositivos</Text>
            </View>
          </TouchableOpacity>
          {/* <TouchableOpacity onPress={_ => handlePress('id')}>
            <View style={styles.col}>
              <Text style={styles[tabs[1]]}>Agregar ID</Text>
            </View>
          </TouchableOpacity> */}
        </View>
        <View style={styles.row2}>
          {tabs[0] === 'textAct' ? (
            <Bluetooth navigation={props.navigation} />
          ) : (
            <AddId navigation={props.navigation} />
          )}
        </View>
      </View>
      {/* <View style={styles.row}>
				<Text style={styles.text1}>Añadir Dispositivo</Text>
			</View>
			<View style={styles.rowTabs}>
				<View style={styles.subRow}>
					<View style={styles.tab}>
						<Button title="Bluetooth" color={state.colorBle} onPress={_ => handlePress('ble')} />
					</View>
					<View style={styles.tab}>
						<Button
							style={styles.tab}
							title="Agregar ID"
							color={state.colorId}
							onPress={_ => handlePress('id')}
						/>
					</View>
				</View>
			</View>

			{state.ble ? <Bluetooth navigation={props.navigation} /> : <AddId navigation={props.navigation} />} */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: vh(12),
  },
  content: {
    backgroundColor: '#ffffff',
    width: vw(89),
    borderRadius: 10,
    borderColor: '#E2E2E2',
    borderWidth: vh(0.15),
  },
  row1: {
    alignItems: 'center',
    justifyContent: 'space-around',
    height: vh(6),
    borderBottomWidth: vh(0.15),
    borderBottomColor: '#E2E2E2',
    flexDirection: 'row',
  },
  text: {
    color: '#424242',
  },
  textAct: {
    color: '#12709E',
  },
});

export default AddDevice;
