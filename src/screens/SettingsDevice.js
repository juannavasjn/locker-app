import React, {useState, useEffect, useCallback} from 'react';
import {View, Text, Button, StyleSheet, TextInput, Picker} from 'react-native';
import Header from '../components/Header';
import {vw, vh, vmin} from 'react-native-expo-viewport-units';
import Footer from '../components/Footer';
import {useSelector, useDispatch} from 'react-redux';

import AwesomeAlert from 'react-native-awesome-alerts';

import {
  readInfo, // read wifi networks: this return one ssid of networks array from device
  writeInfo, // write wifi credentials: {ssid: '', pwd: ''}
} from '../components/ble-manager';

const SettingsDevice = ({navigation}) => {
  const dispatch = useDispatch();
  const currentDevice = useSelector(state => state.general.currentDevice);

  const [deviceName, setDeviceName] = useState('');
  const [deviceId, setDeviceId] = useState('');
  const [networks, setNetworks] = useState([]);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [state, setState] = useState({
    ssid: '',
    pwd: '',
  });

  const setScreen = useCallback(
    payload => dispatch({type: 'SetCurrentScreen', payload}),
    [dispatch],
  );

  useEffect(_ => {
    setScreen('SettingsDevice');
  }, []);

  useEffect(() => {
    if (currentDevice && currentDevice.key) {
      setDeviceName(currentDevice.key);
      setDeviceId(currentDevice.id);
      // console.log('currentDevice.key', currentDevice.key);
      // console.log('deviceId', currentDevice.id);
      getNetworks();
    }
  }, [currentDevice]);

  const getNetworks = async _ => {
    console.log('getNetworks');

    let newNets = [];

    const interval = setInterval(async () => {
      try {
        let net = await readInfo(currentDevice.id);

        if (net !== '***') {
          newNets.push(net);
          setNetworks([...newNets]);
        } else {
          clearInterval(interval);
          // console.log('nets: ');
          // console.log(newNets);
          setNetworks([...newNets]);
        }
      } catch (e) {
        console.log(e);
        clearInterval(interval);
      }
    }, 300);
  };

  const handleSave = async _ => {
    try {
      setAlertMessage('Intentando conectarse a la red WIFI');
      setShowAlert(true);

      await writeInfo(currentDevice.id, state);
      // console.log('Conectado exitosamente a la red WIFI');
      setAlertMessage('Conectado exitosamente a la red WIFI');

      setTimeout(() => {
        setShowAlert(false);
        navigation.navigate('HomeDevice');
      }, 3000);
    } catch (e) {
      setAlertMessage(
        'Ha ocurrido un error al intentar conectarse a la red WIFI',
      );
      setTimeout(() => {
        setShowAlert(false);
      }, 2000);

      console.log('error handleSave');
      console.log(e);
    }
  };

  return (
    <View style={styles.container}>
      <Header title="Configuración" />
      <View style={styles.content}>
        <View style={styles.row1}>
          <View>
            <Text style={styles.text}>Nombre: </Text>
            <TextInput
              style={styles.textInput}
              value={deviceName + ' -  ' + deviceId}
              editable={false}
            />
          </View>
          {/* <View>
              <Text style={styles.text}>ID: </Text>
              <TextInput
                style={styles.textInput}
                value={deviceId}
                editable={false}
              />
            </View> */}

          <View>
            <Text style={styles.text}>Seleccionar Red WIFI: </Text>

            <Picker
              selectedValue={state.ssid}
              onValueChange={(itemValue, itemIndex) =>
                setState({...state, ssid: itemValue})
              }>
              {networks.map((n, i) => (
                <Picker.Item key={i} label={n} value={n} />
              ))}
            </Picker>
          </View>

          <View>
            <Text style={styles.text}>Contraseña: </Text>
            <TextInput
              style={styles.textInput}
              value={state.pasword}
              secureTextEntry={true}
              onChangeText={t => setState({...state, pwd: t})}
              onSubmitEditing={handleSave}
            />
          </View>
          <View>
            <View style={styles.btn}>
              <Button
                title="Guardar"
                color="#1199ED"
                onPress={handleSave}
                // onPress={_ => props.navigation.navigate('HomeDevice')}
              />
            </View>
            {/* <View style={styles.btn}>
						<Button title="Volver" color="#12709E" onPress={_ => props.navigation.navigate('HomeDevice')} />
					</View> */}
          </View>
        </View>
      </View>
      <Footer navigation={navigation} />
      <AwesomeAlert
        show={showAlert}
        showProgress={false}
        message={alertMessage}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
    alignItems: 'center',
    paddingTop: vh(11),
  },
  content: {
    width: vw(89),
  },
  row1: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    borderColor: '#E2E2E2',
    borderWidth: vh(0.15),
    padding: vw(5),
  },
  row2: {
    paddingVertical: vh(3),
  },
  btn: {
    marginVertical: vh(1.4),
    width: vw(77),
  },
  textInput: {
    width: vw(77),
    borderColor: '#424242',
    borderBottomWidth: vw(0.2),
    marginBottom: vh(2),
  },
  text: {
    color: '#424242',
    fontSize: vw(3.5),
    marginTop: vh(0.5),
  },
});

export default SettingsDevice;
