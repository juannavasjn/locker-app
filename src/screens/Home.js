import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import Header from '../components/Header';
import {vw, vh, vmin} from 'react-native-expo-viewport-units';
import Footer from '../components/Footer';
import {useSelector, useDispatch} from 'react-redux';
import Zeroconf from 'react-native-zeroconf';

const zeroconf = new Zeroconf();

let timeout;

const Home = props => {
  const dispatch = useDispatch();

  const allDevices = useSelector(state => state.devices);
  const [doors, setDoors] = useState([]);

  const pushDevice = useCallback(
    payload => dispatch({type: 'PushDevice', payload}),
    [dispatch],
  );

  const setCurrentDevice = useCallback(
    payload => dispatch({type: 'SET_CURRENT_DEVICE', payload}),
    [dispatch],
  );

  const handlePress = e => {
    setCurrentDevice(e);
    props.navigation.navigate('HomeDevice');
  };

  useEffect(_ => {
    zeroconf.scan('http', 'tcp', 'local.');

    clearTimeout(timeout);
    timeout = setTimeout(() => {
      zeroconf.stop();
    }, 5000);

    zeroconf.on('start', () => {
      console.log('started local scanning....');
    });

    zeroconf.on('stop', () => {
      console.log('stopped local scanning....');
    });

    zeroconf.on('resolved', service => {
      // console.log('[Resolve]');
      if (service && service.name === 'lockertouch') {
        const device = {
          key: 'lockertouch.local', // name
          id: service.addresses[0],
          ble: false,
          wifi: true,
          local: true,
        };
        // console.log(device);
        pushDevice(device);
      }
    });
  }, []);

  useEffect(() => {
    setDoors(allDevices);
  }, [allDevices]);

  return (
    <View style={styles.container}>
      <Header title="Inicio" />
      <View style={styles.content}>
        <View style={styles.row1}>
          <Text style={styles.text}>Puertas</Text>
        </View>
        <View style={styles.row2}>
          <FlatList
            data={doors}
            style={styles.list}
            renderItem={({item}) => (
              <TouchableOpacity
                key={item.key}
                style={styles.item}
                onPress={_ => handlePress(item)}>
                <View style={styles.container2}>
                  <Text>{item.key}</Text>
                  <View style={styles.container3}>
                    {item.ble ? (
                      <Image
                        style={styles.imgBle}
                        source={require('../assets/img/ble.png')}
                      />
                    ) : null}

                    {item.wifi ? (
                      <Image
                        style={styles.imgWifi}
                        source={require('../assets/img/wifi.png')}
                      />
                    ) : null}
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
      <Footer navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    backgroundColor: '#ffffff',
    width: vw(89),
    height: vh(75),
    borderRadius: 10,
    borderColor: '#E2E2E2',
    borderWidth: vh(0.15),
  },
  row1: {
    alignItems: 'center',
    justifyContent: 'center',
    height: vh(6),
    borderBottomWidth: vh(0.15),
    borderBottomColor: '#E2E2E2',
  },
  text: {
    color: '#424242',
    fontSize: vmin(4),
  },
  item: {
    padding: 10,
    fontSize: 18,
    marginVertical: 1,
  },
  row2: {
    height: vh(68),
  },
  imgBle: {
    width: vw(2.6),
    height: vw(5),
    marginHorizontal: 20,
  },
  imgWifi: {
    width: vw(6),
    height: vw(4),
  },
  container2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  container3: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
});

export default Home;
