import React, {useEffect, useCallback} from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import Header from '../components/Header';
import {vw, vh, vmin} from 'react-native-expo-viewport-units';
import Footer from '../components/Footer';
import {ws} from '../services/Socket';
import {useDispatch, useSelector} from 'react-redux';
import {writeSwitch} from '../components/ble-manager';
import axios from 'axios';

const HomeDevice = props => {
  const dispatch = useDispatch();

  const currentDevice = useSelector(state => state.general.currentDevice);

  const setScreen = useCallback(
    payload => dispatch({type: 'SetCurrentScreen', payload}),
    [dispatch],
  );

  useEffect(_ => {
    setScreen('HomeDevice');
  }, []);

  const handlePress = async _ => {
    try {
      setScreen('HomeDevice');

      if (currentDevice && currentDevice.ble) {
        console.log('touch by ble');

        await writeSwitch(currentDevice.id, 'touched');
      } else if (currentDevice && currentDevice.local) {
        console.log('touch by local wifi');

        const url = 'http://' + currentDevice.id + '/touched';
        axios
          .get(url)
          .then()
          .catch(err => {
            console.log('Local wifi ERROR');
            console.log(err);
          });
      } else if (currentDevice && currentDevice.wifi) {
        console.log('touch by wifi');

        const message = {
          type: 'openDoor',
          data: currentDevice.id,
        };
        ws.send(JSON.stringify(message));
      }
    } catch (e) {
      console.log('handlePress error:');
      console.log(e);
    }
  };

  return (
    <View style={styles.container}>
      <Header title={currentDevice.key} />
      <View style={styles.content}>
        <TouchableOpacity onPress={handlePress}>
          <View style={styles.btnDoor}>
            <Image
              style={styles.img}
              source={require('../assets/img/main.png')}
            />
          </View>
        </TouchableOpacity>
        <View style={styles.btn}>
          <Button
            title="Configuración"
            onPress={_ => props.navigation.navigate('SettingsDevice')}
            color="#12709E"
          />
        </View>
      </View>
      <Footer navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    width: vw(82),
    height: vh(70),
    borderRadius: 10,
    borderColor: '#ccc',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  btn: {
    width: vw(65),
  },
  btnDoor: {
    width: vw(55),
    height: vw(55),
    borderRadius: vw(70),
    backgroundColor: '#1199ED',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    width: vw(30),
    height: vw(30),
  },
});

export default HomeDevice;
