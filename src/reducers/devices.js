/**
 * devices: [
 *   {
 *    id: string,
 * 	  key: string,
 *    wifi: boolean,
 *    ble: boolean,
 *    local: boolean
 *   }
 * ]
 *
 */

export default (state = [], {type, ...action}) => {
  switch (type) {
    case 'SetDevices':
      return [action.payload];
    case 'PushDevice':
      return [...state, action.payload];
    default:
      return state;
  }
};
