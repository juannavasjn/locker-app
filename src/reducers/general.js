const initialState = {
  currentScreen: 'Home',
  currentDevice: {},
};

export default (state = initialState, {type, ...action}) => {
  switch (type) {
    case 'SET_CURRENT_DEVICE':
      return {
        ...state,
        currentDevice: action.payload,
      };
    case 'SetCurrentScreen':
      return {
        ...state,
        currentScreen: action.payload,
      };
    default:
      return state;
  }
};
