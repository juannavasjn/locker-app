import React, {useCallback} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {vw, vh} from 'react-native-expo-viewport-units';

const Footer = props => {
  const dispatch = useDispatch();
  const setScreen = useCallback(
    payload => dispatch({type: 'SetCurrentScreen', payload}),
    [dispatch],
  );
  const {currentScreen} = useSelector(state => state.general);

  const handlePress = type => {
    setScreen(type);
    props.navigation.navigate(type);
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={_ => handlePress('Home')}>
        <View style={styles.col}>
          {currentScreen === 'Home' ? (
            <Image
              style={styles.img}
              source={require('../assets/img/home2.png')}
            />
          ) : (
            <Image
              style={styles.img}
              source={require('../assets/img/home1.png')}
            />
          )}
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={_ => handlePress('AddDevice')}>
        <View style={styles.col}>
          {currentScreen === 'AddDevice' ? (
            <Image
              style={styles.img}
              source={require('../assets/img/plus2.png')}
            />
          ) : (
            <Image
              style={styles.img}
              source={require('../assets/img/plus1.png')}
            />
          )}
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: '#ffffff',
    height: vh(8.5),
    width: vw(100),
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: vh(0.15),
    borderTopColor: '#E2E2E2',
    flexDirection: 'row',
  },
  col: {
    width: vw(50),
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    width: vw(8),
    height: vw(8),
  },
});

export default Footer;
