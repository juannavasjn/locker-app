import React from 'react';
import { View, Text, Button, StyleSheet, TextInput } from 'react-native';
import { vw, vh, vmin } from 'react-native-expo-viewport-units';

const AddId = props => {
	const handlePress = _ => {
		props.navigation.navigate('SettingsDevice');
	};

	return (
		<View style={styles.container}>
			<View style={styles.row}>
				<Text style={styles.text}>Ingrese ID</Text>
				<TextInput style={styles.textInput} />
			</View>
			<View style={styles.btnRow}>
				<Button style={styles.btn} title="Añadir" color="#1199ED" onPress={handlePress} />
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		height: vh(20)
	},
	btnRow: {
		marginTop: vh(6)
	},
	row: {
		padding: vw(6)
	},
	textInput: {
		borderColor: '#424242',
		borderBottomWidth: 1
		// marginBottom: 15
	},
	text: {
		color: '#424242',
		fontSize: vmin(4)
	}
});

export default AddId;
