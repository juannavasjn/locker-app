import React, {useEffect, useState, useCallback} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  Button,
} from 'react-native';
import {vw, vh, vmin} from 'react-native-expo-viewport-units';
import BleManager from 'react-native-ble-manager';
import {useDispatch} from 'react-redux';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import AwesomeAlert from 'react-native-awesome-alerts';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

const Bluetooth = ({navigation}) => {
  const [devices, setDevices] = useState([]);
  const [showConnectingAlert, setShowConnectingAlert] = useState(false);
  const [showConnectedAlert, setShowConnectedAlert] = useState(false);
  const [alert, setAlert] = useState({
    title: '',
    message: '',
  });

  const dispatch = useDispatch();
  const pushDevice = useCallback(
    payload => dispatch({type: 'PushDevice', payload}),
    [dispatch],
  );

  const setCurrentDevice = useCallback(
    payload => dispatch({type: 'SET_CURRENT_DEVICE', payload}),
    [dispatch],
  );

  const [scanning, setScanning] = useState(false);
  // const allDevices = useSelector(state => state.devices);

  // useEffect(
  //   _ => {
  //     console.log('updated allDevices');
  //     console.log(allDevices);
  //   },
  //   [allDevices],
  // );

  function handleDiscoverPeripheral(peripheral) {
    if (peripheral.name) {
      let newDevice = {
        id: peripheral.id,
        key: peripheral.name,
        // serviceUUIDs: peripheral.advertising.serviceUUIDs,
        connected: false,
      };

      let duplicate = devices.filter(e => e.id === newDevice.id);

      if (duplicate.length === 0) {
        setDevices([...devices, newDevice]);
      }
    }
  }

  useEffect(() => {
    // const handlerDisconnect = bleManagerEmitter.addListener(
    //   'BleManagerDisconnectPeripheral',
    //   handleDisconnectedPeripheral,
    // );
    // const handlerUpdate = bleManagerEmitter.addListener(
    //   'BleManagerDidUpdateValueForCharacteristic',
    //   handleUpdateValueForCharacteristic,
    // );

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      ).then(result => {
        if (result) {
          console.log('Permission is OK');
          startBluetooth();
        } else {
          PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          ).then(result => {
            if (result) {
              console.log('User accept');
              startBluetooth();
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }

    // BleManager.scan([], 60);

    // BleManager.disconnect('D1:8D:93:0A:D8:10')
    //   .then(() => {
    //     // Success code
    //     console.log('Disconnected');
    //   })
    //   .catch(error => {
    //     // Failure code
    //     console.log(error);
    //   });
  }, []);

  const verifyLocation = _ =>
    new Promise((resolve, reject) => {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      })
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          console.log('ERROR verifyLocation: ');
          console.log(err);

          reject(err);
        });
    });

  const startScan = _ => {
    BleManager.scan([], 10);
    setScanning(true);
    console.log('new scan...');
  };

  const startBluetooth = async _ => {
    await verifyLocation();

    BleManager.start({showAlert: false}).then(() => {
      console.log('Module initialized');
    });

    BleManager.enableBluetooth()
      .then(() => {
        // Success code
        console.log('The bluetooth is already enabled or the user confirm');

        //---------------------------- scan bluetooth devices -----------------
        startScan();
        //---------------------------------
      })
      .catch(error => {
        // Failure code
        console.log('The user refuse to enable bluetooth');
      });

    bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      handleDiscoverPeripheral,
    );

    bleManagerEmitter.addListener('BleManagerStopScan', handleStopScan);
  };

  const handleStopScan = _ => {
    // console.log('handleStopScan');
    setScanning(false);
  };

  const connect = dev => {
    if (dev.key === 'Buscando . . .') return;

    console.log('---- conectando a ', dev.id);

    // alert(`Conectando a ${dev.key}...`);
    setAlert({
      ...alert,
      message: `Conectando a ${dev.key}, por favor espere...`,
    });
    setShowConnectingAlert(true);

    BleManager.connect(dev.id).then(() => {
      console.log('---- conectado a ', dev.id);

      // retrieve the services advertised by this peripheral
      BleManager.retrieveServices(dev.id).then(_ => {
        setShowConnectingAlert(false);
        //------------------------------------------------------
        let devsUpdated = devices.map(e => {
          if (e.id === dev.id) {
            e.connected = true;
            return e;
          } else {
            return e;
          }
        });
        setDevices(devsUpdated);

        const device = {
          key: dev.key, // name
          id: dev.id,
          ble: true,
          wifi: false,
        };
        pushDevice(device);

        setCurrentDevice(device);

        setAlert({
          ...alert,
          message: `Conectado exitosamente a ${
            dev.key
          }. \n\n¿Desea conectar el dispositivo a la red WIFI?`,
        });

        setTimeout(_ => {
          setShowConnectedAlert(true);
        }, 100);

        //-------------------------------------------------------
      });
    });
  };

  return (
    <View style={styles.list}>
      <FlatList
        data={devices}
        renderItem={({item}) => (
          <TouchableOpacity
            key={item.key}
            style={styles.item}
            onPress={_ => connect(item)}>
            <Image
              style={styles.img}
              source={require('../assets/img/locker.png')}
            />
            <Text style={styles.text}>{item.key}</Text>
          </TouchableOpacity>
        )}
      />

      {scanning ? (
        <View style={styles.bleControl}>
          <Text>Escaneando...</Text>
        </View>
      ) : (
        <View style={styles.bleControl}>
          <View style={styles.btnScan}>
            <Button
              color="#12709E"
              title="Buscar dispositivos"
              onPress={startScan}
            />
          </View>
        </View>
      )}
      <AwesomeAlert
        show={showConnectingAlert}
        showProgress={false}
        message={alert.message}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
      />
      <AwesomeAlert
        show={showConnectedAlert}
        showProgress={false}
        message={alert.message}
        closeOnTouchOutside={false}
        closeOnHardwareBackPress={false}
        showCancelButton={true}
        showConfirmButton={true}
        cancelText="No"
        confirmText="Si"
        onCancelPressed={() => {
          setShowConnectedAlert(false);
          navigation.navigate('HomeDevice');
        }}
        onConfirmPressed={() => {
          setShowConnectedAlert(false);
          navigation.navigate('SettingsDevice');
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    fontSize: 18,
    height: vh(7),
    marginHorizontal: vw(3),
    flexDirection: 'row',
  },
  list: {
    marginTop: 10,
    marginBottom: 4,
    // maxHeight: vh(40),
    height: vh(40),
  },
  text: {
    color: '#424242',
    fontSize: vmin(4),
  },
  img: {
    // width: vw(2),
    // height: vw(2),
    width: 14,
    height: 20,
    marginRight: 15,
    marginLeft: 10,
  },
  bleControl: {
    backgroundColor: '#f7f7f7',
    paddingHorizontal: 20,
    paddingVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  btnScan: {
    width: vw(50),
    fontSize: 10,
  },
});

export default Bluetooth;
