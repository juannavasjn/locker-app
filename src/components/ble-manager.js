import BleManager from 'react-native-ble-manager';
import {stringToBytes} from 'convert-string';
import bytesCounter from 'bytes-counter';
import {Buffer} from 'buffer';

// construct the UUIDs the same way it was constructed in the server component earlier
const BASE_UUID = '-5659-402b-aeb3-d2f7dcd1b999';
const PERIPHERAL_ID = '0000';
const PRIMARY_SERVICE_ID = '0100';

// the service UUID
const primary_service_uuid = PERIPHERAL_ID + PRIMARY_SERVICE_ID + BASE_UUID;
// write wifi credentials: {ssid: '', pwd: ''}
const ps_characteristic_uuid = PERIPHERAL_ID + '0300' + BASE_UUID;
// read wifi networks
const ps_characteristic_read_uuid = PERIPHERAL_ID + '0400' + BASE_UUID;
// write switch: turn on or turn off (gpio)
const ps_characteristic_switch_uuid = PERIPHERAL_ID + '0700' + BASE_UUID;

export const readInfo = deviceId =>
  new Promise((resolve, reject) => {
    BleManager.read(deviceId, primary_service_uuid, ps_characteristic_read_uuid)
      .then(readData => {
        // console.log('Read: ');

        const buff = Buffer.from(readData);
        const data = buff.toString('utf8');
        // console.log(readData);

        resolve(data);
      })
      .catch(error => {
        // Failure code
        console.log('ERROR readInfo:');
        console.log(error);
        reject(error);
      });
  });

/**
 * deviceId: mac address
 * data: {ssid: '', pwd: ''}
 */
export const writeInfo = (deviceId, data) =>
  new Promise((resolve, reject) => {
    const str = JSON.stringify(data);
    const length = bytesCounter.count(str);
    const bytes = stringToBytes(str);

    BleManager.write(
      deviceId,
      primary_service_uuid,
      ps_characteristic_uuid,
      bytes,
      length,
    )
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        // Failure code
        console.log('ERROR writeInfo:');
        console.log(error);
        reject(error);
      });
  });

/**
 * deviceId: mac address
 * data: 'touched'
 */
export const writeSwitch = (deviceId, data) =>
  new Promise((resolve, reject) => {
    const str = JSON.stringify(data);
    const length = bytesCounter.count(str);
    const bytes = stringToBytes(str);

    BleManager.write(
      deviceId,
      primary_service_uuid,
      ps_characteristic_switch_uuid,
      bytes,
      length,
    )
      .then(res => {
        resolve(res);
      })
      .catch(error => {
        // Failure code
        console.log('ERROR writeSwitch:');
        console.log(error);
        reject(error);
      });
  });
