import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { vw, vh, vmin } from 'react-native-expo-viewport-units';

const Header = props => {
	return (
		<View style={styles.container}>
			<Text style={styles.title}>{props.title}</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		position: 'absolute',
		top: 0,
		left: 0,
		backgroundColor: '#ffffff',
		height: vh(8),
		width: vw(100),
		justifyContent: 'center',
		alignItems: 'center',
		borderBottomWidth: vh(0.15),
		borderBottomColor: '#E2E2E2'
	},
	title: {
		fontSize: vmin(5),
		color: '#424242'
	}
});

export default Header;
