/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {View} from 'react-native';
import Navigator from './navigation/Navigator';
import Socket from './services/Socket';
import {createStore, combineReducers} from 'redux';
import general from './reducers/general.js';
import devices from './reducers/devices.js';
import {Provider} from 'react-redux';

const reducer = combineReducers({
  general,
  devices,
});

const store = createStore(reducer);

const App = () => {
  return (
    <Provider store={store}>
      <View style={{flex: 1}}>
        <Socket />
        <Navigator />
      </View>
    </Provider>
  );
};

export default App;
