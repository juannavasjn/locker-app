import React, {useEffect, useCallback} from 'react';
import ReconnectingWebSocket from 'react-native-reconnecting-websocket';
import {getUniqueId, getBrand, getDeviceName} from 'react-native-device-info';
import {useDispatch, useSelector} from 'react-redux';

// const url = 'ws://192.168.1.46:3020';

const url = 'wss://lockertouch.juannavas.dev';

const ws = new ReconnectingWebSocket(url);

// const ws = new WebSocket(url);

let devices = [];

const Socket = _ => {
  const dispatch = useDispatch();
  const allDevices = useSelector(state => state.devices);

  const pushDevice = useCallback(
    payload => dispatch({type: 'PushDevice', payload}),
    [dispatch],
  );

  useEffect(
    _ => {
      devices = allDevices;
    },
    [allDevices],
  );

  useEffect(_ => {
    ws.onopen = async () => {
      let id = await getUniqueId();
      let device = await getBrand();
      let idName = await getDeviceName();

      console.log('connected . . . ');
      let data = {
        type: 'idPhone',
        data: {
          name: device + ' ' + idName,
          id,
        },
      };

      console.log(data);

      ws.send(JSON.stringify(data));
    };

    ws.onmessage = e => {
      // console.log('onmessage');
      // console.log(e.data);
      const parse = JSON.parse(e.data);
      const payload = parse.data;
      // console.log(payload);
      switch (parse.type) {
        case 'devices':
          payload.forEach(d => {
            if (d.type === 'door') {
              const exists = devices.find(dev => dev.id == d.id);

              if (!exists) {
                const device = {
                  key: d.name, // name
                  id: d.id,
                  ble: false,
                  wifi: true,
                  local: false,
                };
                // console.log(device);
                pushDevice(device);
              }
            }
          });
          break;
        default:
          break;
      }
    };

    ws.onclose = e => {
      console.log('disconnect . . .');
      // this.props.connect(false);
      // //console.warn('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
      // setTimeout(() => {
      // 	this.connect(device);
      // }, 1000);
    };
  }, []);

  console.log('socket . . .');
  return null;
};

export {ws};

export default Socket;
